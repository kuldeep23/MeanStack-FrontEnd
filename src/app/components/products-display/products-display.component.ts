import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductView } from 'src/app/models/ProductView';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-products-display',
  templateUrl: './products-display.component.html',
  styleUrls: ['./products-display.component.scss']
})
export class ProductsDisplayComponent implements OnInit {


  public products: ProductView[] = [] as ProductView[]
  public errorMessage: string | undefined;

  constructor(private productService: ProductService,
    private router: Router) {
  }
  ngOnInit(): void {
   
    this.productService.listProduct().subscribe((data) => {
      console.log(data, "data=====")
      this.products = data as ProductView[];

    }, (error) => {
      this.errorMessage = error;
    })
  }

  //navigate to Add Product Page
  public addProductPage() {
    this.router.navigate(['products/add'])
  }

  //Delete Product
  public clickDeleteProduct(productId: string | undefined) {
    if (productId) {
      this.productService.deleteProduct(productId).subscribe((data: any) => {

        this.productService.listProduct().subscribe((data: any) => {
          this.products = data as ProductView[];

        })
      }, (error) => {
        this.errorMessage = error;
      });
    }
  }

}
