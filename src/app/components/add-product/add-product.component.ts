import { Component, OnInit } from '@angular/core';
import { ProductView } from 'src/app/models/ProductView';
import { ProductService } from 'src/app/services/product/product.service';
import {Router} from "@angular/router"

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit{

  public product:ProductView={
    _id:"",
    name: "",
    price: "",
    quantity: "",
    info: "",
  }

  public isEmptyFields:boolean | undefined;

  constructor(private productService:ProductService ,private router:Router){}

  ngOnInit():void{

  }

  //Submit Product
  public submitCreateProduct(){
    if(this.product.name !=="" && this.product.price !=="" && this.product.quantity !=="" && this.product.info !=="" )
   {
    this.productService.createProduct(this.product).subscribe((result:any)=>{
      this.router.navigate(["/products/list"]).then()
    })
   }
   else{}
    this.isEmptyFields=true;
  }
}
