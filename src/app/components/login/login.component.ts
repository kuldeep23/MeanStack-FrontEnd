import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { AuthInterceptor } from 'src/app/interceptors/auth.interceptor';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;

  public errorMessage: string | undefined;

  public isEmptyFields: boolean | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private http: HttpClient,
    private router: Router,) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: "",
      password: ""
    })

  }
 
  public submitUserLogin() {
    
    if (this.form.value!=="") {
      this.userService.loginUser(this.form.getRawValue()).subscribe((response: any) => {      
        AuthInterceptor.accessToken = response.token;
        this.router.navigate(["/products/list"])
      }, (error) => {
        this.errorMessage = error;
      });
    }
    // else {}
    //   this.isEmptyFields = true;  
  }

}
