import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductView } from 'src/app/models/ProductView';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  public productId: string | null = "";
  public selectedProduct: ProductView = {
    _id:"",
    name: "",
    price: "",
    quantity: "",
    info: "",
  };

  public errorMessage: string | undefined;
  public isEmptyFields: boolean | undefined;

  constructor(private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((param: ParamMap) => {
      this.productId = param.get('productId')
      console.warn(this.productId, "productID")
    })
    if (this.productId) {
      this.productService.getProduct(this.productId).subscribe((data: any) => {
        this.selectedProduct = data;
      }, (error) => {
        this.errorMessage = error;
      });

    }
  }

  //Update Product
  public submitUpdateProduct() {
    if (this.selectedProduct.name !== "" && this.selectedProduct.price !== "" && this.selectedProduct.quantity !== "" && this.selectedProduct.info !== "") {
      if (this.productId) {
        this.productService.updateProduct(this.productId, this.selectedProduct).subscribe((result: any) => {
          this.router.navigate(["/products/list"]).then()
        })
      }
    }
    else { }
    this.isEmptyFields = true;
  }


}
