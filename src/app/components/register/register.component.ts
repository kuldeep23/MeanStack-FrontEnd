import { Component, OnInit } from '@angular/core';
// import {FormBuilder,Validators} from "@angular/forms"
import { Router } from "@angular/router"
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/User';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public userForm: User = {
    firstName: "",
    lastName: "",
    email: "",
    password: ""
  };
  public errorMessage: string | undefined;

  public isEmptyFields: boolean | undefined;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  public submitUserRegister() {

    if (this.userForm.firstName !== "" && this.userForm.lastName !== "" && this.userForm.email !== "" && this.userForm.password !== "") {
      this.userService.registerUser(this.userForm).subscribe((result: any) => {
        this.router.navigate(["/"]).then()
      }, (err) => {
        this.errorMessage = err;
      })
    }

  }
}


