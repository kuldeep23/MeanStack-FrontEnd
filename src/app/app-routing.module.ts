import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { ProductsDisplayComponent } from './components/products-display/products-display.component';
import { AddProductComponent } from './components/add-product/add-product.component'
import { RegisterComponent } from "./components/register/register.component"
import { LoginComponent } from "./components/login/login.component"
import { NavbarComponent } from './components/navbar/navbar.component';

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "products/list", component: ProductsDisplayComponent },
  { path: "products/add", component: AddProductComponent },
  { path: "products/:productId", component: EditProductComponent },
  { path: "user/register", component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
