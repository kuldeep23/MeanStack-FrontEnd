import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { ProductView } from '../../models/ProductView';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  constructor(private http: HttpClient) { }

  public createProduct(product: ProductView) { 
    const postURL = "api/v1/products";
    return this.http.post(postURL,product)
  }

  public updateProduct(productId: string, product: ProductView) {
    const updateURL = `api/v1/products/${productId}`;
    return this.http.put(updateURL, product)
  }

  public listProduct() {
    const getAllURL = "api/v1/products";
    return this.http.get(getAllURL).pipe(map(response => response as ProductView[]))
  }

  public getProduct(productId: string) {
    const getURL = `api/v1/products/${productId}`
    return this.http.get(getURL)
  }

  public deleteProduct(productId: string) {
    const deleteURL = `api/v1/products/${productId}`
    return this.http.delete(deleteURL)
  }
}
