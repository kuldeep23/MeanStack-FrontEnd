import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { User } from '../../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public registerUser(user:User) {
    const postURL = "api/v1/register";
    return this.http.post(postURL,user)
  }

  public loginUser(user:any) {
    
    const postURL = "api/v1/login";
    return this.http.post(postURL,user)
  }

}
